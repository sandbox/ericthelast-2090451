(function($) {
  var tcapi_base_url = "https://api.onthecity.org/docs/admin#";
  Drupal.behaviors.tcapi = {
    attach: function(context, settings) {
      $(function() {
        var method = $("#edit-method").val();
        if(method.length > 0) {
          updateDocsLink(method);
        } else {
          updateDocsLink("");
        }
      })
      $("#edit-method").change(function() {
        updateDocsLink($(this).val());
      })
    }
  }

  function updateDocsLink(method) {
    $("#docs-link").attr("href", tcapi_base_url + method);
  }
})(jQuery);
