<?php

/**
 * @file
 * Admin callbacks for The City Admin API module.
 *
 * @ingroup tcapi
 */

/**
 * Admin settings form.
 */
function tcapi_admin_settings_form($form, &$form_state) {
  $form['tcapi_city_api_key'] = array(
    '#type' => 'textfield',
    '#title' => t('Your City API Key'),
    '#description' => t(
      'Enter the "Secret Key" obtained from the !link on The City.', array(
        '!link' => l(
          t('API Settings'),
          'https://journeyon.onthecity.org/admin/api/city_api_keys?subnav_state=settings_api_keys',
          array('attributes' => array('target' => '_blank'))
        ),
      )
    ),
    '#default_value' => variable_get('tcapi_city_api_key', ''),
  );

  $form['tcapi_city_api_token'] = array(
    '#type' => 'textfield',
    '#title' => t('Your City User Token'),
    '#description' => t(
      'Enter the "User Token" obtained from the !link on The City.',
      array(
        '!link' => l(
          t('API Settings'),
          'https://journeyon.onthecity.org/admin/api/city_api_keys?subnav_state=settings_api_keys',
          array('attributes' => array('target' => '_blank'))
        ),
      )
    ),
    '#default_value' => variable_get('tcapi_city_api_token', ''),
  );

  return system_settings_form($form);
}
