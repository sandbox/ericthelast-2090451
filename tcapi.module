<?php

/**
 * @file
 * Main file for The City Admin API module.
 *
 * This module provides a front-end interface to The City's admin api. This is
 * a developer's module and should only be installed/enabled to explore The
 * City's Admin API. Care should be given when assigning permissions to use
 * the front-end as sensitive data could be exposed to users with access.
 */

/**
 * Implements hook_menu().
 */
function tcapi_menu() {
  $items['tcapi'] = array(
    'title' => 'The City Admin API',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tcapi_api_form'),
    'access arguments' => array('access the city api'),
  );

  $items['admin/config/development/tcapi'] = array(
    'title' => 'The City API Settings',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('tcapi_admin_settings_form'),
    'access arguments' => array('access the city api'),
    'file' => 'tcapi.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function tcapi_permission() {
  return array(
    'access the city api' => array(
      'title' => t('Access The City API Interface'),
      'restrict access' => TRUE,
      'warning' => t('Give to trusted roles only; this permission exposes users to all information accessible by The City Admin API and could potentially provide access to sensitive data.'),
    ),
  );
}

/**
 * The City Admin API form.
 */
function tcapi_api_form($form, &$form_state) {
  $module_path = drupal_get_path('module', 'tcapi');
  form_load_include($form_state, 'php', 'tcapi', 'thecity-admin-php/lib/ca-main');
  $methods = get_class_methods('CityApi');
  sort($methods);

  $form_state['#tcapi_city_api_key'] = variable_get('tcapi_city_api_key');
  $form_state['#tcapi_city_api_token'] = variable_get('tcapi_city_api_token');
  if (empty($form_state['#tcapi_city_api_key']) || empty($form_state['#tcapi_city_api_token'])) {
    drupal_set_message(t('Calls will probably fail since you have not set either your city api key or token.  Click !link to configure.', array('!link' => l(t('here'), 'admin/config/development/tcapi'))));
  }

  $form['method'] = array(
    '#type' => 'select',
    '#title' => t('Method'),
    '#options' => drupal_map_assoc($methods),
    '#required' => TRUE,
    '#description' => '<a href="javascript:;" target="_blank" id="docs-link">' . t('View Method Documentation') . '</a>',
    '#attached' => array(
      'js' => array(
        $module_path . '/js/tcapi.js' => array(),
      ),
    ),
  );

  $form['args'] = array(
    '#type' => 'textarea',
    '#title' => t('Arguments'),
    '#description' => t('A json array of parameters that will be passed to the method. PHP is very strict so ensure proper formatting (no trailing commas).  For example, if a method takes one argument, enter [12345] (with the brackets).  See !link for help with JSON sytnax.', array('!link' => l(t('this link'), 'http://www.w3schools.com/json/'))),
  );

  $functions = array(
    'print_r' => 'print_r',
    'var_dump' => 'var_dump',
    'serialize' => 'serialize (php)',
  );
  if (module_exists('devel')) {
    $functions['dsm'] = 'Devel (dsm)';
    $default_function = 'dsm';
  }
  else {
    $default_function = 'print_r';
  }

  $form['return_function'] = array(
    '#type' => 'select',
    '#title' => t('Return Function'),
    '#description' => t('How the results of the method call should be rendered/displayed.'),
    '#options' => $functions,
    '#default_value' => $default_function,
    '#required' => TRUE,
  );

  $form['json_decode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Run json_decode on result?'),
    '#default_value' => TRUE,
  );

  $form['actions'] = array(
    '#type' => 'actions',
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Submit'),
    ),
  );

  return $form;
}

/**
 * City Admin API form submission callback.
 */
function tcapi_api_form_validate($form, &$form_state) {
  $ca = new CityApi();
  $ca->set_key($form_state['#tcapi_city_api_key']);
  $ca->set_token($form_state['#tcapi_city_api_token']);

  if (!empty($form_state['values']['args'])) {
    $args = json_decode($form_state['values']['args'], TRUE);
    if (json_last_error() != JSON_ERROR_NONE) {
      form_set_error('args', t('There was a problem with your json. Make sure you use double quotes instead of single quotes, and no trailing commas.'));
      return;
    }
  }
  else {
    $args = NULL;
  }
  try {
    if ($args) {
      $result = call_user_func_array(array($ca, $form_state['values']['method']), $args);
    }
    else {
      $result = call_user_func(array($ca, $form_state['values']['method']));
    }
    if ($form_state['values']['json_decode']) {
      $result = json_decode($result);
    }
    switch ($form_state['values']['return_function']) {
      case 'dsm':
        dsm($result);
        break;

      case 'print_r':
        dsm(print_r($result, TRUE));
        break;

      case 'var_dump':
        var_dump($result);
        break;

      case 'serialize':
        dsm(serialize($result));
        break;

    }
  }
  catch(Exception $e) {
    drupal_set_message(t($e), 'error');
  }

  // Here we set an error to prevent form submission so that
  // all entered form values are still present on each page load.
  form_set_error('form', '');
}
